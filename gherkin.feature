+-------------+        +-------------+
|    Gymnase  |        |   Séance    |
+-------------+        +-------------+
| id          |        | id          |
| nom         |        | type_sport  |
| adresse     |        | horaire     |
| telephone   |        | capacite    |
+-------------+        +-------------+
        |                      |
        |                      |
+-------------+        +-------------+
|   Membre    |        |    Coach    |
+-------------+        +-------------+
| id          |        | id          |
| nom         |        | nom         |
| prenom      |        | prenom      |
| adresse     |        | age         |
| date_naissance|       | specialite |
| sexe        |        +-------------+
+-------------+                |
        |                      |
+-------------+        +-------------+
| Inscription |        | Anime      |
+-------------+        +-------------+
| id_membre   |        | id_seance   |
| id_seance   |        | id_coach    |
+-------------+        +-------------+